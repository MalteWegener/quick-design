function getMff(LD, V, R, CJ)
{
    //10 percent of range added for reasons
    var b = V/(9.80665*CJ);
    var c = LD;
    var a = R;

    /*console.log("a: " + a);
    console.log("b: " + b);
    console.log("V: " + V);
    console.log("c: " + c);*/

    console.log("cj: " + CJ);

    console.log(Math.exp(-a/(b*c)))

    return Math.exp(-a/(b*c))
    
}

function getWeight(Payload, Mff)
{
    var slope = 0.4171;
    var intercept = 17921.9375;
    console.log((1-slope-Mff));
    return (intercept+Payload)/(-slope+Mff)
}

function estimateweight()
{
    document.getElementById("out").textContent = "";
    //we just hardcode some stuff in because we have no reason to load it from a database until we don't have a server running
    var basefuelfractions = 0.99*0.99*0.995*0.98*0.99*0.992;

    //now lets get user input for range and payload
    try
    {
        var rgh = parseFloat(document.getElementById("rangeharmonic").value);
        var plh = parseFloat(document.getElementById("plharmonic").value);
        var rgd = parseFloat(document.getElementById("rangedesign").value);
        var pld = parseFloat(document.getElementById("pldesign").value);
        var cjt = parseFloat(document.getElementById("cj").value)/1000000;
        var ldt = parseFloat(document.getElementById("ld").value);
        var vt = parseFloat(document.getElementById("v").value);
        console.log(ld);
    }
    catch
    {
        alert("error");
    }
    /*if(isNaN(rgh) || isNaN(plh) || isNaN(rgd) || isNaN(pld) || isNaN(cjt) || isNaN(ld) || isNaN(v))
    {
        alert("invalid entry");
    }*/ 
    console.log("MFF" + basefuelfractions*getMff(ldt,vt,rgh*1000,cjt))
    var result = Math.max(getWeight(plh,basefuelfractions*getMff(ldt,vt,rgh*1000,cjt)),getWeight(pld,basefuelfractions*getMff(ldt,vt,rgd*1000,cjt)));
    result = Math.round(result);
    document.getElementById("out").textContent = result;
}