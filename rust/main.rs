struct PrelimAirplane{
    slope: f64,
    intercept: f64,
    cj: f64,
    speed: f64,
    LD: f64
}

fn CalculateMTOW(plane: & PrelimAirplane, Range: f64, Payload: f64) -> i64{
    //We specifiy some parameters early
    let bff = 0.99*0.99*0.995*0.98*0.99*0.992;

    let a = Range *1.1 * 1000.0; //this is the range for the formula
    let b = plane.speed/(9.80665*plane.cj); //Other Part of the Eqation
    let c = plane.LD;
    let Mff = bff * (-a/(b*c)).exp();

    ((plane.intercept+Payload)/(-plane.slope+Mff)).round() as i64
}

fn main(){
    println!("Initializing");

    let cruisespeed: f64 = 903.0;
    let specificFuel: f64 = 0.000019;
    let glideratio = 15.0;
    let slp = 0.4171;
    let ict = 17921.9375;

    println!("Your aircraft Data is:");
    println!("Cj: {} kg/Ns",specificFuel);
    println!("Cruise Speed: {} m/s", cruisespeed);
    println!("L/D: {}", glideratio);

    println!("Aircraft will be Initialized");
    let base = PrelimAirplane{slope: slp,intercept: ict,cj: specificFuel,speed: cruisespeed,LD: glideratio};

    let HarmonicRange: f64 = 10400.0;
    let HarmonicPayload: f64 = 55000.0;
    println!("Your Harmonic Range is {} km at a Payload of {} kg", HarmonicRange,HarmonicPayload);

    let DesignRange: f64 = 17000.0;
    let DesignPayload: f64 = 20000.0;
    println!("Your Design Range is {} km at a Payload of {} kg", DesignRange,DesignPayload);

    println!("Ranges Calibrated, starting calculations");

    let MTOWHarmonic = CalculateMTOW(&base, HarmonicRange, HarmonicPayload);
    let MTOWDesign = CalculateMTOW(&base, DesignRange, DesignPayload);

    println!("The MTOW for the Harmonic Range is {} kg", MTOWHarmonic);
    println!("The MTOW for the Design Range is {} kg", MTOWDesign);

    println!("Therefore the MTOW is: {} kg", std::cmp::max(MTOWDesign, MTOWHarmonic));
}